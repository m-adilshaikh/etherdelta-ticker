ws_edelta_uri = 'https://api.forkdelta.com:443'
ws_edelta = io(ws_edelta_uri, {
  transports: ['websocket'],
  forceNew: true
})

var token_addr = '0x41104c8d51a80d9e2969dc23256a352de4b0ec33'
var token_name = '0x41104c8' // PLUM

function fetchPrice(currency) {
  return axios.get('https://api.coinmarketcap.com/v1/ticker/ethereum/', {port: 443}).then(function(res) {
    var data = res.data[0]
    var price = data['price_'+currency.toLowerCase()]
    console.log(price)
    return price
  })
}

function updateSubTicker(currency, parent_data) {
  currency = currency.toLowerCase()
  var el_ticker = document.querySelector('.ticker .pair[data-market="'+currency+'"]')
  
  fetchPrice(currency).then(function(price) {

    // Multiply
    Object.keys(parent_data).forEach(function(key) {
      if(!isNaN(parent_data[key])  && !!parseFloat(parent_data[key], 10)) {
        console.log(parent_data[key])
        parent_data[key] *= price
        parent_data[key] = Math.round(parent_data[key] * Math.pow(10, 6)) / Math.pow(10, 6)
      }
    })

    updateTickerView(currency, parent_data)
  })
}

function updateTickerView(currency, data) {
  currency = currency.toLowerCase()
  Object.keys(data).map(function(key) {
    var el = document.querySelector('.ticker .pair[data-market="'+currency+'"] [data-var="'+key+'"]')
    if(el) {
      el.style.opacity = 0.4
      var val = (data[key].toString().includes('e')) ? parseFloat(data[key]).toFixed(9) : data[key]
      el.innerHTML = val
      window.setTimeout(function(){
        el.style.opacity = 1
      }, 300)
    }
  })
}

// ticker fetch delay in seconds
var TICKER_DELAY = 60
var preDataCount = 0

ws_edelta.on('connect', function() {
  console.log('# [etherdelta] socket connected')
})

ws_edelta.on( 'disconnect', function () {
  console.log( '[etherdelta] socket disconnected')
})

ws_edelta.on( 'reconnecting', function () {
  console.log( '[etherdelta] retrying socket connection')
})


function getMarket() {
  if(ws_edelta.connected) {
    console.log('# Getting PLUM data')
    ws_edelta.emit('getMarket', {token: token_addr})
  }
}

window.onload = function() {

  var el_ticker = document.querySelector('.ticker')
  var el_price = document.querySelector('.ticker .pair[data-market="eth"] .price')

  ws_edelta.on('market', function(data) {

    console.log(data)
    var tickers = data.returnTicker

    if(Object.keys(tickers).length !== 0) {
      ticker = tickers['ETH_'+token_name]
      if(el_ticker.classList.contains('delayed'))
        el_ticker.classList.remove('delayed')
    }

    else {
      console.log('Ticker not received')
      preDataCount++
      if(preDataCount > 3) {
        el_ticker.classList.add('delayed')
      }

      return
    }

    console.log(ticker)

    if(el_ticker.classList.contains('waiting'))
      el_ticker.classList.remove('waiting')

    // Update View
    console.log('# Updating PLUM Ticker')

    updateTickerView('ETH', ticker)

    if(ticker.percentChange > 0) {
      el_price.classList.remove('down')
      el_price.classList.add('up')
    }
    else if (ticker.percentChange < 0) {
      el_price.classList.remove('up')
      el_price.classList.add('down')
    }

    // Update PLUM / USD
    updateSubTicker('USD', ticker)
  })

  getMarket()
  window.setInterval(getMarket, TICKER_DELAY*1000)

}
