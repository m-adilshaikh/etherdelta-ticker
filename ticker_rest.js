var token_addr = '0x41104c8d51a80d9e2969dc23256a352de4b0ec33'
var token_name = '0x41104c8' // PLUM

function fetchPrice(currency) {
  return axios.get('https://api.coinmarketcap.com/v1/ticker/ethereum/', {port: 443}).then(function(res) {
    var data = res.data[0]
    var price = data['price_'+currency.toLowerCase()]
    return price
  })
}

function updateSubTicker(currency, parent_data) {
  currency = currency.toLowerCase()
  var el_ticker = document.querySelector('.ticker .pair[data-market="'+currency+'"]')
  
  fetchPrice(currency).then(function(price) {

    // Multiply
    Object.keys(parent_data).forEach(function(key) {
      if(typeof parent_data[key] == 'number') {
        parent_data[key] *= price
        parent_data[key] = Math.round(parent_data[key] * Math.pow(10, 6)) / Math.pow(10, 6)
      }
    })

    updateTickerView(currency, parent_data)
  })
}

function updateTickerView(currency, data) {
  currency = currency.toLowerCase()
  Object.keys(data).map(function(key) {
    var el = document.querySelector('.ticker .pair[data-market="'+currency+'"] [data-var="'+key+'"]')
    if(el) {
      el.style.opacity = 0.4
      var val = (data[key].toString().includes('e')) ? data[key].toFixed(9) : data[key]
      el.innerHTML = val
      window.setTimeout(function(){
        el.style.opacity = 1
      }, 300)
    }
  })
}

function handleData(data) {

  console.log(data)
  var tickers = data.returnTicker

  if(Object.keys(tickers).length !== 0) {
    ticker = tickers['ETH_'+token_name]
    if(el_ticker.classList.contains('delayed'))
      el_ticker.classList.remove('delayed')
  }

  else {
    console.log('Ticker not received')
    preDataCount++
    if(preDataCount > 3) {
      el_ticker.classList.add('delayed')
    }

    return
  }

  console.log(ticker)

  if(el_ticker.classList.contains('waiting')) {
    el_ticker.classList.remove('waiting')
  }

  console.log('# Updating PLUM Ticker')

  updateTickerView('ETH', ticker)

  if(ticker.percentChange > 0) {
    el_price.classList.remove('down')
    el_price.classList.add('up')
  }
  else if (ticker.percentChange < 0) {
    el_price.classList.remove('up')
    el_price.classList.add('down')
  }

  updateSubTicker('USD', ticker)
}


// ticker fetch delay in seconds
var TICKER_DELAY = 5
var preDataCount = 0

window.onload = function() {

  var el_ticker = document.querySelector('.ticker')
  var el_price = document.querySelector('.ticker .pair[data-market="eth"] .price')

}

window.setInterval(function() {
  console.log('# Getting PLUM data')
  // ws_edelta.emit('getMarket', {token: token_addr})

  // axios.get('https://api.forkdelta.com/returnTicker', {
  //   port: 443,
  //   headers: {
  //     'Access-Control-Allow-Origin': '*',
  //   }
  // }).then(function(res) {
  //   var data = res
  //   console.log(res)
  // })

  $.ajax({
    url: 'https://api.forkdelta.com/returnTicker',
    method: 'GET',
    dataType: 'json',
    success: function(data) {
      console.log(data)
    }
  })

}, TICKER_DELAY*1000)

